/* 
    Task1: 

    Возьмите любую Free Rest Api, и с помощью объекта XMLHttpRequest выполните POST запрос согласно документации этого API. 

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Читайте внимательно документацию самой API, дабы понять что должно отправляться при POST запросе.

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

const sendNewPost = (body, successResponse) => {
    const postsUrl = 'https://jsonplaceholder.typicode.com/posts';
    const xhr = new XMLHttpRequest();

    xhr.open('POST', postsUrl);

    xhr.addEventListener('load', () =>  {
        successResponse(JSON.parse(xhr.response));
    });

    xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');

    xhr.send(JSON.stringify(body));
};

const successRequired = response => {
    console.log(response);
};

const newPost = {
    title: 'Новый пост',
    body: 'Тело поста',
    userId: 1,
};

sendNewPost(newPost, successRequired);



/* 
    Task2:

    Возьмите любую понравившуюся вам Free Rest API, и выполните Get запрос с помощью метода fetch()

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

const url = 'https://jsonplaceholder.typicode.com/posts';

fetch(url)
    .then(response => response.json())
    .then(posts => console.log(posts))
    .catch(e => console.log(e));


/* 
    Task3:
    
    Возьмите API с урока, с сайта jsonplaholder, и получите с сервера с помощью GET запроса комментарий с id 31. Пускай этот комментарий выводится в консоль по нажатию на кнопку которая будет у вас в html

*/

const btn = document.getElementById('btn');

const commentsUrl = 'https://jsonplaceholder.typicode.com/comments/';

const getComment = id => {
    return new Promise((resolve,reject) => {
        fetch(commentsUrl + id)
            .then(response => response.json())
            .then(comment => resolve(comment))
            .catch(e => reject(e));
    });
};

btn.addEventListener('click', () => {
    getComment(31)
        .then(comment => console.log(comment))
        .catch(e => console.log(e));
});

/* 
    Task4:
  
    Дан код:

    const getNumber = n => {
        return new Promise(r => {
            setTimeout(() => {
                r(n);
            },2000)
        })
    }

    const printNumber = n => {
        return new Promise(r => {
            getNumber(n).then(res => {
                r(res)
            })
        })
    }

    printNumber(100).then(data => console.log(data));


    Перепишите его используя операторы async/await
*/

const getNumber = num => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(num);
        },2000);
    });
};

async function printNumber(n) {
    const data = await getNumber(n);
    console.log(data);
};  

printNumber(100);